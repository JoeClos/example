
package examsandquestions;

import java.time.LocalDate;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ExamRepository  extends JpaRepository<Exam, Long> {
   List<Exam> findByExamDate (LocalDate examDate); 
} 
